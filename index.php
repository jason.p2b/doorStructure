<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Vue.js in PHP</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
  <style>
    .blue {color: blue;}
    .red {color: red;}
    .green{color:green;}

    .svgShape {
      stroke: #000;
      fill: none;
    }
    .svgText {
      fill:#f00;
      text-anchor:middle;
      font-size: 9px;
    }
    .svgTextBackground {
      stroke-width: 2; stroke:#fff; fill:#fff;
    }
    .svgMeasure {
      stroke:#3c3; 
      fill:none;
    }
  </style>
</head>

<body>
  <div id="app">
    <div class="row">
      <div class="col-2">
        <Ig type="text" v-model="site">工地</Ig>
        <Ig type="text" v-model="model">型號</Ig>
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <div class="input-group-text" for="is_mother">單/雙</div>
          </div>
          <select class="form-control" name="is_mother" id="is_mother" v-model="is_mother">
            <option value="單">單</option>
            <option value="母">母</option>
            <option value="子">子</option>
          </select>
        </div>
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <div class="input-group-text" for="type">類別</div>
          </div>
          <select class="form-control" name="type" id="type" v-model="type">
            <option value="防火">防火門</option>
            <option value="玄關">玄關門</option>
          </select>
        </div>
        <Ig type="number" v-model="width">寬度</Ig>
        <Ig type="number" v-model="height">高度</Ig>
        <Ig type="number" v-model="amount_l">數量L</Ig>
        <Ig type="number" v-model="amount_r">數量R</Ig>
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <div class="input-group-text" for="lock">鎖</div>
          </div>
          <select class="form-control" name="lock" id="lock" v-model="lock">
            <option value="CN-26K">CN-26K</option>
          </select>
        </div>
        <Ig type="number" v-model="lock_height">鎖高度</Ig>
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <div class="input-group-text" for="handle">把手</div>
          </div>
          <select class="form-control" name="handle" id="handle" v-model="handle">
            <option value="凹槽">凹槽</option>
          </select>
        </div>
        <Ig type="number" v-model="handle_height">把手高度</Ig>
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <div class="input-group-text" for="hinge">鉸鍊</div>
          </div>
          <select class="form-control" name="hinge" id="hinge" v-model="hinge">
            <option value="CN-240">CN-240</option>
            <option value="OK-910">OK-910</option>
            <option value="CN-180">CN-180</option>
            <option value="Power 地鉸鍊">Power 地鉸鍊</option>
            <option value="加安地鉸鍊">加安地鉸鍊</option>
            <option value="CN-1050A x 2">CN-1050A x 2</option>
            <option value="CN-1050A x 3">CN-1050A x 3</option>
            <option value="CN-1050A x 4">CN-1050A x 4</option>
            <option value="CN-1050A x 5">CN-1050A x 5</option>
            <option value="CN-1050L x 2">CN-1050L x 2</option>
            <option value="CN-1050L x 3">CN-1050L x 3</option>
            <option value="CN-1050L x 4">CN-1050L x 4</option>
            <option value="CN-1050L x 5">CN-1050L x 5</option>
            <option value="CN-1044A x 2">CN-1044A x 2</option>
            <option value="CN-1044A x 3">CN-1044A x 3</option>
            <option value="CN-1044A x 4">CN-1044A x 4</option>
            <option value="CN-1044A x 5">CN-1044A x 5</option>
            <option value="CN-127 x 2">CN-127 x 2</option>
            <option value="CN-127 x 3">CN-127 x 3</option>
          </select>
        </div>
        <Ig v-if="computedHingeAmount >= 1" type="number" v-model="hinge1">鉸鍊 1 位置</Ig>
        <Ig v-if="computedHingeAmount >= 2" type="number" v-model="hinge2">鉸鍊 2 位置</Ig>
        <Ig v-if="computedHingeAmount >= 3" type="number" v-model="hinge3">鉸鍊 3 位置</Ig>
        <Ig v-if="computedHingeAmount >= 4" type="number" v-model="hinge4">鉸鍊 4 位置</Ig>
        <Ig v-if="computedHingeAmount >= 5" type="number" v-model="hinge5">鉸鍊 5 位置</Ig>
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <div class="input-group-text" for="top">上加工</div>
          </div>
          <select class="form-control" name="top" id="top" v-model="top">
            <option value="-">無</option>
            <option value="外掛式門弓器">外掛式門弓器</option>
            <option value="隱藏式門弓器">隱藏式門弓器</option>
          </select>
        </div>
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <div class="input-group-text" for="bottom">下加工</div>
          </div>
          <select class="form-control" name="bottom" id="bottom" v-model="bottom">
            <option value="-">無</option>
            <option value="外掛式下降條">外掛式下降條</option>
            <option value="隱藏式下降條">隱藏式下降條</option>
          </select>
        </div>
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <div class="input-group-text" for="material">材質</div>
          </div>
          <select class="form-control" name="material" id="material" v-model="material">
            <option value="SECC 1.2t">SECC 1.2t</option>
            <option value="SUS 1.2t PVC">SUS 1.2t PVC</option>
            <option value="SUS 1.2t 2B">SUS 1.2t 2B</option>
          </select>
        </div>
      </div>
      <div class="col-10">
        <div class="row">
          <button class="btn btn-primary" onclick="printDiv('door-info')">Print</button>
        </div>
        <div id="door-info" class="row">
          <table id="table" class="table">
            <thead>
              <tr>
                <th>工地</th>
                <th>型號</th>
                <th>尺寸</th>
                <th>數量</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{ site }}</td>
                <td>{{ computedModel }} </td>
                <td>{{ width }} x {{ height }}</td>
                <td>{{ amount }}</td>
              </tr>
            </tbody>
            <thead>
              <tr>
                <th>門鎖</th>
                <th>鉸鍊</th>
              </tr>
            </thead>
            <tbody>
                <tr>
                  <td>{{ computedLock }}</td>
                  <td>{{ computedHinge }}</td>
                </tr>
            </tbody>
            <thead>
              <tr>
                <th>上加工</th>
                <th>下加工</th>
                <th>材質</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{ top }}</td>
                <td>{{ bottom }}</td>
              　<td>{{ material }}</td>
              </tr>
            </tbody>
          </table>

          <svg id="svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 835 1200" style="font-family:Arial;">
            <defs>
              <g id="five-hole">
                 <circle cx="0" cy="0" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="10" cy="0" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="5" cy="10" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="0" cy="20" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="10" cy="20" r="2" style="stroke:#00f; fill:none;" />
              </g>
            </defs>
            <defs>
              <g id="six-hole">
                 <circle cx="0" cy="0" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="10" cy="0" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="0" cy="10" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="10" cy="10" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="0" cy="20" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="10" cy="20" r="2" style="stroke:#00f; fill:none;" />
              </g>
            </defs>
            <defs>
              <g id="butterfly-hole">
                 <circle cx="10" cy="-10" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="0" cy="-5" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="10" cy="0" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="0" cy="5" r="2" style="stroke:#00f; fill:none;" />
                 <circle cx="10" cy="10" r="2" style="stroke:#00f; fill:none;" />
              </g>
            </defs>
            <defs>
              <g id="top-down-hole">
                <circle cx="0" cy="10" r="2" style="stroke:#00f; fill:none;" />
                <circle cx="10" cy="0" r="2" style="stroke:#00f; fill:none;" />
                <circle cx="20" cy="10" r="2" style="stroke:#00f; fill:none;" />
                <circle cx="30" cy="0" r="2" style="stroke:#00f; fill:none;" />
              </g>
            </defs>
            <g id="outline">
              <rect x="0" y="0" height="1000" width="800" style="stroke:#999; fill:none;"/>

              <!-- temp guideline -->
              <line x1="250" y1="0" x2="250" y2="1000" style="stroke:#999; fill:none;"/>
              <line x1="550" y1="0" x2="550" y2="1000" style="stroke:#999; fill:none;"/>
              <line x1="0" y1="200" x2="800" y2="200" style="stroke:#999; fill:none;"/>
              <line x1="0" y1="800" x2="800" y2="800" style="stroke:#999; fill:none;"/>

              <!-- 40x20 扁管 -->
              <g transform="translate(0,0)">
                <text transform="translate(10,20)">40mm x 20mm 扁管</text>
                <text transform="translate(20,40)">- {{ boneHeight }} mm x {{ 2 * (amount_l + amount_r) }}支</text>
                <text transform="translate(20,60)">- {{ boneWidth }} mm x {{ 2 * (amount_l + amount_r) }}支</text>
                <text v-if="this.bottom == '外掛式下降條'" transform="translate(20,80)">- {{ boneWidth - 100}} mm x {{ amount_l + amount_r }}支</text>
              </g>

              <!-- 4mm 矽酸鈣板 -->
              <g transform="translate(550,0)">
                <text transform="translate(10,20)">4mm 矽酸鈣板</text>
                <text transform="translate(20,40)">- 24 mm x {{ boneHeight - 2 }} x {{ 4 * (amount_l + amount_r) }}支</text>
                <text transform="translate(20,60)">- {{  }} mm x {{ boneHeight - 2 }} x {{ 4 * (amount_l + amount_r) }}支</text>
              </g>

              <!-- 骨架 -->
              <g transform="translate(275,250)">
                <text transform="translate(-15,-15)">外</text>
                <text transform="translate(120,270)">內</text>
                <rect x="0" y="0" height="20" width="250" style="stroke:#f00; fill:none;"/>
                <text transform="translate(120,15)">{{ boneWidth }}</text>
                <line />
                <rect x="0" y="480" height="20" width="250" style="stroke:#f00; fill:none;"/>
                <text transform="translate(120,495)">{{ boneWidth }}</text>
                <rect x="0" y="20" height="460" width="20" style="stroke:#f00; fill:none;"/>
                <text transform="translate(5,150) rotate(90)">{{ boneHeight }}</text>
                <rect x="230" y="20" height="460" width="20" style="stroke:#f00; fill:none;"/>
                <text transform="translate(235,150) rotate(90)">{{ boneHeight }}</text>

                <!-- 加工 -->
                <rect v-if="this.top == '外掛式門弓器'" x="150" y="20" height="35" width="80" style="stroke:#00f; fill:none;"/>
                <rect v-if="this.bottom == '外掛式下降條'" x="30" y="460" height="20" width="190" style="stroke:#00f; fill:none;"/>
                <text v-if="this.bottom == '外掛式下降條'" transform="translate(120,475)">{{ boneWidth - 100}}</text>

                <!-- 門鎖 -->
                <g v-if="" transform="translate(20,250)">
                  <rect x="0" y="0" height="50" width="40" style="stroke:#00f; fill:none;"/>
                  <rect x="170" y="0" height="50" width="40" style="stroke:#00f; fill:none;"/>
                  <circle cx="20" cy="25" r="10" style="stroke:#00f; fill:none;"/>
                </g>
                <g v-if="lock == '5115'" transform="translate(20,250)">
                  <rect x="0" y="0" height="50" width="30" style="stroke:#00f; fill:none;"/>
                  <circle cx="15" cy="15" r="5" style="stroke:#00f; fill:none;"/>
                  <path transform="translate(7,32)" d="M0,5 8,0 16,5 8,10Z" style="stroke:#00f; fill:none;"/>
                </g>
                <g v-if="lock == 'blab'" transform="translate(20,250)">
                  <rect x="0" y="0" height="50" width="30" style="stroke:#00f; fill:none;"/>
                  <circle cx="15" cy="15" r="5" style="stroke:#00f; fill:none;"/>
                  <path transform="translate(7,32)" d="M0,5 8,0 16,5 8,10Z" style="stroke:#00f; fill:none;"/>
                </g>

              </g>

              <!-- 鎖邊 骨架 側邊 -->
              <g transform="translate(10,250)">
                <rect x="40" width="40" y="20" height="460" style="stroke:#f00; fill:none;"/>
                <rect x="120" width="40" y="20" height="460" style="stroke:#f00; fill:none;"/>
                <text transform="translate(53,50)">外</text>
                <text transform="translate(133,50)">內</text>
                <ML length="460" :label="boneHeight" :position=[90,20,90] level=1 />
                <ML length="250" :label="boneLockHeight" :position=[20,20,90] level=-1 />
                <ML length="250" :label="boneLockHeight" :position=[160,20,90] level=2 />
                <ML length="40" label="40" :position=[40,20,0] level=1 />
                <ML length="40" label="40" :position=[120,20,0] level=1 />

                <!-- 平推 不須開 -->

                <g><!-- 水平 -->
                  <!-- 外 -->
                  <rect x="42" width="36" y="245" height="50" style="stroke:#00f; fill:none;"/>
                  <ML length="100" :label="150" :position=[20,170,90] level=1 />
                  <rect x="42" width="36" y="145" height="50" style="stroke:#00f; fill:none;"/>
                  <ML length="50" :label="120" :position=[50,245,90] level=1 />
                  <ML length="50" :label="120" :position=[50,145,90] level=1 />
                  <!-- 內 -->
                  <rect x="122" width="36" y="252" height="36" style="stroke:#00f; fill:none;"/>
                  <ML length="100" :label="150" :position=[160,170,90] level=1 />
                  <rect x="122" width="36" y="152" height="36" style="stroke:#00f; fill:none;"/>
                  <ML length="36" :label="60" :position=[130,252,90] level=1 />
                  <ML length="36" :label="60" :position=[130,152,90] level=1 />
                </g>

                <g v-if="lock == ''"><!-- 水平 J5115 -->
                  <!-- 外 -->
                  <rect x="42" width="36" y="200" height="100" style="stroke:#00f; fill:none;"/>
                  <ML length="70" :label="150" :position=[20,200,90] level=1 />
                  <ML length="30" :label="70" :position=[20,270,90] level=1 />
                  <!-- 內 -->
                  <rect x="122" width="36" y="200" height="100" style="stroke:#00f; fill:none;"/>
                  <ML length="70" :label="150" :position=[160,200,90] level=1 />
                  <ML length="30" :label="70" :position=[160,270,90] level=1 />
                </g>
              </g>

              <!-- 鉸鍊邊 骨架 側邊 -->
              <g transform="translate(600,250)">
                <rect x="40" width="40" y="20" height="460" style="stroke:#f00; fill:none;"/>
                <text transform="translate(53,40)">外</text>
                <ML length="40" label="40" :position=[40,20,0] level=1 />
                <ML length="460" :label="boneHeight" :position=[90,20,90] level=1 />

                <!-- 5孔旗型 -->
                <g>
                  <Hh
                    :hingetype="hingeType"
                    :amount="computedHingeAmount"
                    :hingepos="boneHingeHeight"
                    :horizontalpos="boneHingeInOut"
                    :position=[50,60,0]
                  />
                </g>
              </g>
              <!-- 上橫料 鉸鍊 -->
              <g transform="translate(275,0)">
                <rect x="0" y="100" height="40" width="250" style="stroke:#f00; fill:none;"/>
                <ML length="250" :label="boneWidth" :position=[0,160,0] />
                <ML length="40" label="40" :position=[-20,100,90] />
                <!-- 上下 鉸鍊 -->
                <g v-if="hingeType == 'top-down-hole'">
                  <use xlink:href="#top-down-hole" x="200" y="110" />
                  <ML length="20" label="18.5" :position=[230,100,0] />
                  <ML length="10" :label="boneHingeInOut" :position=[230,100,90] />
                </g>
                <!-- 地鉸鍊 -->
                <g v-if="hinge == 'CN-180'" transform="translate(200,110)">
                  <circle cx="0" cy="0" r="2" style="stroke:#00f; fill:none;" />
                  <circle cx="15" cy="0" r="2" style="stroke:#00f; fill:none;" />
                  <circle cx="30" cy="0" r="2" style="stroke:#00f; fill:none;" />
                  <ML length="20" label="34.5" :position=[30,-10,0] />
                  <ML length="10" :label="15.5 - 4 - panelThickness" :position=[30,-10,90] />
                </g>
              </g>

              <!-- 下橫料 鉸鍊 -->
              <g transform="translate(275,800)">
                <rect x="0" y="60" height="40" width="250" style="stroke:#f00; fill:none;"/>
                <ML length="250" :label="boneWidth" :position=[0,120,0] />
                <ML length="40" label="40" :position=[-20,60,90] />
                <!-- 上下 鉸鍊 -->
                <g v-if="hingeType == 'top-down-hole'">
                  <use xlink:href="#top-down-hole" x="200" y="70" />
                  <ML length="20" label="18.5" :position=[230,60,0] />
                  <ML length="10" :label="boneHingeInOut" :position=[230,60,90] />
                </g>
                <!-- 地鉸鍊 -->
                <g v-if="hinge == 'CN-180'" transform="translate(150,70)">
                  <circle cx="0" cy="0" r="2" style="stroke:#00f; fill:none;" />
                  <circle cx="30" cy="0" r="2" style="stroke:#00f; fill:none;" />
                  <circle cx="60" cy="0" r="2" style="stroke:#00f; fill:none;" />
                  <ML length="40" label="67.5" :position=[60,-10,0] />
                  <ML length="10" :label="9.5 - 4 - panelThickness" :position=[60,-10,90] />
                </g>
              </g>

            </g>
          </svg>
        </div>
      </div>
    </div>
  </div>
<script type="module">
  import App from "./components/App.js";  
  Vue.createApp(App).mount('#app');
</script>
<script>
function printDiv(divId) {
    var divContent = document.getElementById(divId).innerHTML;

    var printWindow = window.open('', '_blank');

    // Set styles for A4 size paper
    var styles = `
        <style>
            body {
                width: 210mm;
                height: 297mm;
                margin: 0;
                padding: 20mm;  /* Adjust as needed */
                font-size: 12pt;  /* Adjust as needed */
                font-family: Arial, sans-serif;  /* Adjust as needed */
            }
        </style>
    `;

    printWindow.document.write('<html><head><title>Print</title>' + styles + '</head><body>');
    printWindow.document.write(divContent);
    printWindow.document.write('</body></html>');
    
    printWindow.document.close(); 
    printWindow.onload = function() {
        printWindow.print();
    };
}


</script>
</body>
</html>