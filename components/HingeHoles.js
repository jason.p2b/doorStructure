import MeasureLabel from './MeasureLabel.js';

export default {
    components: {
        Ml: MeasureLabel
    },

    template: `
    <g :id="myID" :transform="svgPos">
        <use v-if="amount > 0" :xlink:href="type" x="0" y="0" />
        <use v-if="amount >= 3" :xlink:href="type" x="0" y="60" />
        <use v-if="amount >= 4" :xlink:href="type" x="0" y="120" />
        <use v-if="amount >= 5" :xlink:href="type" x="0" y="220" />
        <use v-if="amount > 0" :xlink:href="type" x="0" y="320" />
        <Ml v-if="amount > 0" length="40" :label="hingepos[0]" :position=[-30,-40,90] />
        <Ml v-if="amount==2" length="320" :label="hingepos[1]" :position=[-30,0,90] />
        <Ml v-if="amount>=3" length="60" :label="hingepos[1]" :position=[-30,0,90] />
        <Ml v-if="amount==3" length="260" :label="hingepos[2]" :position=[-30,60,90] />
        <Ml v-if="amount>=4" length="60" :label="hingepos[2]" :position=[-30,60,90] />
        <Ml v-if="amount==4" length="200" :label="hingepos[3]" :position=[-30,120,90] />
        <Ml v-if="amount>=5" length="100" :label="hingepos[3]" :position=[-30,120,90] />
        <Ml v-if="amount==5" length="100" :label="hingepos[4]" :position=[-30,220,90] />
        <Ml v-if="amount > 0" length="10" :label="horizontalpos" :position=[-10,0,0] />
    </g>
    `,

    props: {
        myID: {
            type: String,
            default: ""
        },
        hingetype: {
            type: String,
            default: ""
        },
        amount: {
            type: Number,
            default: 0
        },
        hingepos: {
            type: Array,
            default: [0,0,0,0,0]
        },
        horizontalpos: {
            type: Number,
            default: 0
        },
        position: {
            type: Array,
            default: [0,0,0]
        },
    },

    computed: {
        svgPos() {
            return "translate(" + this.position[0] + "," + this.position[1] + ") rotate(" + this.position[2] + ")";
        },
        type() {
            return '#' + this.hingetype;
        },
        
    },

}