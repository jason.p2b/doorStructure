import InputGroup from './InputGroup.js';
import MeasureLabel from './MeasureLabel.js';
import HingeHoles from './HingeHoles.js';

export default {
	components: {
		Ig: InputGroup,
		Ml: MeasureLabel,
		Hh: HingeHoles,
	},
	data() {
		return {
			'site': 'TCBD A1棟',
			'model': 'SD1',
			'is_mother': '單',
			'type': "防火",
			'height': 2033,
			'width': 900,
			'amount_l': 0,
			'amount_r': 0,
			'lock': "CN21-01S",
			'lock_height': 1033,
			'handle': "",
			'handle_height': 0,
			'hinge_amount': 0,
			'hinge': "",
			'hinge1': 0,
			'hinge2': 0,
			'hinge3': 0,
			'hinge4': 0,
			'hinge5': 0,
			'material': 'SECC 1.2t',
			'top': '-',
			'bottom': '-',
		}
	},
	computed: {
		computedModel() {
			if (this.is_mother != "單") {
				return this.model + this.is_mother + " / " + this.type;
			}
			return this.model + " / " + this.type;
		},
		amount() {
			let amount = "";
			if (this.amount_l != 0) {
				amount = "L" + this.amount_l;
			}
			if (this.amount_r != 0) {
				amount = amount + " R" + this.amount_r;
			}
			return amount;
		},
		computedLock() {
			let result = "";
			if (this.lock != "") {
				result = this.lock + ' / ' + this.lock_height;
			}
			if (this.handle != "") {
				result = result + ' | ' + this.handle + ' / ' + this.handle_height;
			}
			return result;
		},
		computedHingeAmount() {
			const match = /x (\d+)/.exec(this.hinge);
			return match ? parseInt(match[1], 10) : 0;
		},
		computedHinge() {
			let result = this.hinge;

			if ( this.computedHingeAmount >= 1) {
				result = result + ' / ' + this.hinge1;
			}
			if ( this.computedHingeAmount >= 2) {
				result = result + ' / ' + this.hinge2;
			}
			if ( this.computedHingeAmount >= 3) {
				result = result + ' / ' + this.hinge3;
			}
			if ( this.computedHingeAmount >= 4) {
				result = result + ' / ' + this.hinge4;
			}
			if ( this.computedHingeAmount >= 5) {
				result = result + ' / ' + this.hinge5;
			}

			return result;
		},
		boneWidth() {
			return this.width - 3 - 4 - 4;
		},
		boneHeight() {
			let minus = 75; // 3+20+20+16+16;
			
			if (this.top == "隱藏式門弓器") {
				minus -= 8;
			}
			if (this.bottom == "隱藏式下降條") {
				minus += 16;
			}

			return this.height - minus;
		},
		boneLockHeight() {
			let height = this.lock_height - 20 - 1.5; // 扣上橫扁管

			if (this.top == "隱藏式門弓器") {
				height = height - 8;
			} else {
				height = height - 16;
			}

			return height;
		},
		boneHingeHeight() {
			let height = this.hinge1 - 20 - 1.5; // 扣上橫扁管

			if (this.top == "隱藏式門弓器") {
				height = height - 8;
			} else {
				height = height - 16;
			}

			return [height,this.hinge2,this.hinge3,this.hinge4,this.hinge5];
		},
		lockType() {
			return;
		},
		hingeType() {
			if (this.hinge.indexOf('CN-1050A') !== -1 || 
				this.hinge.indexOf('Power 105') !== -1 || 
				this.hinge.indexOf('OK 601') !== -1) {
				return "five-hole";
			}

			if (this.hinge.indexOf('CN-1050L') !== -1 || 
				this.hinge.indexOf('Power 106') !== -1) {
				return "six-hole";
			}

			if (this.hinge.indexOf('CN-1044A') !== -1 || 
				this.hinge.indexOf('Power 215B') !== -1) {
				return "butterfly-hole";
			}

			if (this.hinge.indexOf('CN-240') !== -1 || 
				this.hinge.indexOf('OK-910') !== -1) {
				return "top-down-hole";
			}

			if (this.hinge.indexOf('CN-180') !== -1 ) {
				return "earth-hole";
			}

			return "";
		},
		boneHingeInOut() {
			let panelThickness = 0;

			if (this.type == "玄關") {
				panelThickness = 3;
			} else {
				panelThickness = 2;
			}

			if (this.hingeType == "five-hole") {
				return 14 - panelThickness - 4;
			}

			if (this.hingeType == "six-hole") {
				return 12 - panelThickness - 4;
			}

			if (this.hingeType == "butterfly-hole") {
				return 20 - panelThickness - 4;
			}

			if (this.hingeType == "top-down-hole") {
				return 9 - panelThickness - 4;
			}

			if (this.hingeType == "earth-hole") {
				return 15.5 - panelThickness - 4;
			}

			return 0;
		},
		panelThickness() {
			if (this.type == "玄關") {
				return 3;
			} 

			return 2;
		}
	}
}