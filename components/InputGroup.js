export default {
    template: `
    <div class="input-group mb-1">
        <span class="input-group-text"><slot /></span>
        <input 
            
            class="form-control" 
            :type="type" 
            :disabled="disabled == 1"
            v-model.lazy="value"
        />
    </div>    
    `,

    props: ['modelValue', 'type', 'disabled'],
    emits: ['update:modelValue'],
    computed: {
        value: {
          get() {
            return this.modelValue
          },
          set(value) {
            this.$emit('update:modelValue', value)
          }
        }
    }

}